/**
 * Created by @aloncarmel & @omriariav on 3/27/15.
 */
$(document).ready( function() {
    $('div, span, img, button, input').click(function(e) {
        trackButton(e);
    });

    var optionsFunc = function() {
        var event = document.createEvent('Event');
        event.initEvent('options');
        document.dispatchEvent(event);
    };

    //attach the comment api to the button
    var broadcastid =  getUrlVars()["b"];

    checkIfCanActions(broadcastid);

    $.ajaxSetup({ cache: false });

    $('#broadcastiframe').attr('src','http://meerkatapp.co/boardcast/'+broadcastid);

    getSummary(broadcastid);

    //TODO Add orientation button to flip video on the side
    //TODO Add send to a friend button

    $('#like_btn').on('click', function() {
        api.like(broadcastid);
        getActivies(broadcastid);
        getWatchers(broadcastid);
        $('#like_btn').fadeOut('slow');

    });

    $('#comment_btn').on('click', function() {
        api.comment($('#comment_text').val(),broadcastid);
        $('#comment_text').val('');
        getActivies(broadcastid);
        getWatchers(broadcastid);

    });
    $('#rt_btn').on('click', function() {
        api.retweet(broadcastid);
        getActivies(broadcastid);
        getWatchers(broadcastid);
        $('#rt_btn').fadeOut('slow');
    });
    $(document).keypress(function(e) {
        if(e.which == 13) {
            trackSubmit();
            api.comment($('#comment_text').val(),broadcastid);

            $('#comment_text').val('');
            getActivies(broadcastid);
            getWatchers(broadcastid);

        }
    });

    $('#randommeerkat').on('click', function() {


        getRandomMeerkat();


    });


});

function checkIfCanActions(broadcastid) {
    _user_id = localStorage.getItem("ext_userid") || "";
    $.getJSON('http://resources.meerkatapp.co/broadcasts/'+broadcastid+'/summary', function(data) {
        $.each(data.result.influencers, function(k,v) {
            _meta = v.split(":");
            _type = _meta[0];
            _influ_id = _meta[1];
            if (_type.toLowerCase() == "r" && _influ_id == _user_id) {
                $('#rt_btn').css('display', 'none');
            }
            if (_type.toLowerCase() == "l" && _influ_id == _user_id) {
                $('#like_btn').css('display', 'none')
            }
        })
    });
}


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getRandomMeerkat() {


    var broadcasts = 'http://resources.meerkatapp.co/broadcasts';

    $.getJSON(broadcasts,function(json) {

        var min = 0;
        var max = json.result.length-1;
        // and the formula is:
        var random = Math.floor(Math.random() * (max - min + 1) + min);
        var selected = json.result[random];
        console.log(selected);
        var win = window.open('http://meerkatapp.co/broadcast/'+selected.id, '_blank');
        win.focus();

    });

}


function getSummary(broadcastid) {

    var summaryurl = 'http://resources.meerkatapp.co/broadcasts/'+broadcastid+'/summary';

    $.getJSON(summaryurl,function(json) {

        $(document).data('broadcaster',json.result.broadcaster);
        $('#boardcastername').html('<img class="watcherthumbchat watcherthumb" src="'+json.result.broadcaster.image+'"> '+json.result.broadcaster.displayName);
        if(json.result.caption) {
            $('#boardcasttitle').html(' '+json.result.caption);
        }

        if(json.result.status == 'ended') {

            $('#commentrow').hide();
        }

        getActivies(broadcastid);
        getWatchers(broadcastid);
        setInterval('getActivies("'+broadcastid+'")', 5000);
        setInterval('getWatchers("'+broadcastid+'")', 10000);


    })

}

function getActivies(broadcastid) {

    var activitiessurl = 'https://resources.meerkatapp.co/broadcasts/'+broadcastid+'/activities';
    $.getJSON(activitiessurl, function(activitiesjson) {

        $('#comments').empty();
        var activity = activitiesjson.result.activities;

        for (var i = activity.length - 1; i >= 0; i--) {


            $('#comments').append('<div class="commentwrapper" data-username="'+activity[i].watcher.username+'" data-message="'+activity[i].message+'"><img class="watcherthumb" src="'+activity[i].watcher.profileThumbImage+'"><p class="commentlabel"><span class="commentlabelspan"><span class="commentername">@'+activity[i].watcher.username+'</span> '+activity[i].message+'</span></p></div>');
        }

        $('.commentwrapper').unbind( "click" );

        $('.commentwrapper').click(function(event){
            var broadcaster =  $(document).data('broadcaster');
            var message = $(this).data('message');
            openTweet('@'+broadcaster.name+' '+message,broadcastid,activitiesjson.result.tweetId);

        });

    });

}

function getWatchers(broadcastid) {

    var watchersurl = 'https://resources.meerkatapp.co/broadcasts/'+broadcastid+'/watchers';

    $.getJSON(watchersurl,function(watchersjson) {
        $('#watchers').empty();
        var watchers = watchersjson.result.watchers;

        for (var i = watchers.length - 1; i >= 0; i--) {

            $('#watchers').append('<a href="http://meerkatapp.co/'+watchers[i].username+'" target="_blank"><img class="watcherthumblist" data-toggle="tooltip" data-placement="top" title="'+watchers[i].displayName+'" src="'+watchers[i].profileThumbImage+'" text="'+watchers[i].displayName+'"></a>&nbsp;');
        }
        $("#watchers img,#comments img").error(function () {
            _egg = chrome.extension.getURL('icons/egg.jpg');
            $(this).attr('src',_egg);
        });
    });




}

function openTweet(text,broadcastid,tweetid) {

    var win = window.open('https://twitter.com/intent/tweet?text='+text+'&url=http://meerkatapp.co/broadcast/'+broadcastid+'&hashtags=meerkat&in-reply-to='+tweetid, '_blank');
    win.focus();

}
