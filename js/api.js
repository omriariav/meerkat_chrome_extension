/**
 * Created by @aloncarmel & @omriariav on 3/27/15.
 */

//Our consts
var _current_meerkat_api_version = "0.1.2";
var _auth = localStorage.getItem("ext_auth") || "0";
var _url = "https://channels.meerkatapp.co/broadcasts/";
var _suffix = "?auth=" + _auth + "&v=" + _current_meerkat_api_version;
var api = {

    init: function() {
    },

    //Generic ajax function to use
    //TODO: add callback functionality to check if success or fail
    _ajax: function(url, payload, callback) {
        if (payload == {}) {
            payload = '';
        }
        $.ajax({
            url: url,
            headers: {
                'Accept':'application/json',
                'Content-Type':'application/json',
                'MeerkatPlus': 'ChromeExtension_1_3_3'
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(payload),
            success: function(data){
            }
        });
    },

    //Payload for comments is {"auth":auth token,"body": comment body
    //TODO add input check (length and not null)
    comment :function(text,boardcastid) {
        if(text) {
            _comment_url = _url +boardcastid+ "/comments" + _suffix;
            this._ajax(_comment_url,
                {
                    "auth": this._auth,
                    "body": text
                });
        }

    },
    like: function(broadcastid) {
        _like_url = _url + broadcastid + "/likes" + _suffix;
        this._ajax(_like_url,
            {
                //"auth": this._auth,
                //"body": 'liked'
            });

    },
    retweet: function(broadcastid) {
        _rt_url = _url + broadcastid + "/restreams" + _suffix
        this._ajax(_rt_url,
            {
                "type": "restream and retweet"
            });
    },
    follow: function(meerkatUserId) {
        _follow_url = "https://social.meerkatapp.co/users/" + meerkatUserId + "/followers" + "?v="
            + _current_meerkat_api_version;
        this._ajax(_follow_url,
            {
                "auth": this.auth
            });
    },
    live_broadcasts: function() {
        _live_broadcasts_url = "https://resources.meerkatapp.co/broadcasts";
        $.getJSON(_live_broadcasts_url, function(data) {
            return data;
        });
    },
    upcoming_broadcasts: function() {
        _upcoming_broadcasts_url = "https://resources.meerkatapp.co/schedules";
        $.getJSON(_upcoming_broadcasts_url, function(data) {
            return data.result
        })

    },

    search: function(username) {

        //POST https://social.meerkatapp.co/users/USERID/followers?v=1&auth=fc72583f-9f81-4471-8526-85ef5518ddd8
        //PUT SEARCH with { username: } returns the user id https://social.meerkatapp.co/users/search?v=0.1
        //GET https://resources.meerkatapp.co/users/USERID/profile?v=web

        $.ajax({
          url: 'https://social.meerkatapp.co/users/search?v=0.1',
          type: 'PUT',
          data: "username="+username,
          success: function(data) {
           
            console.log(data);

          }
        });

    }
};

