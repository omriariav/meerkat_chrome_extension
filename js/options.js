$(document).ready( function() {
    _notificationSettings = localStorage.getItem('_notificationSettings') || '1';
    if (_notificationSettings == '1') {
        $('#notificationsSettings').val($(this).is(':checked'));
    }
    else {
        $('#notificationsSettings').attr('checked', false);
    }
    $('#notificationsSettings').change(function() {
        if($(this).is(":checked")) {
            localStorage.setItem('_notificationSettings','1');
            _gaq.push(['_trackEvent', 'notificationsOn', 'notification']);
        }
        else {
            _gaq.push(['_trackEvent', 'notificationsOff', 'notification']);
            localStorage.setItem('_notificationSettings', '0');
        }
    });
});