/**
 * Created by @aloncarmel & @omriariav on 3/27/15.
 */

//Sends a message to the bg.js to open the homepage.html and passes
//the needed parameters
var openClient = function(data) {
    msg = {
        type: 'broadcastdata',
        auth: data._auth,
        broadcast_id: data._broadcast_id,
        userid: data._user
    };
    chrome.runtime.sendMessage(
        msg,
        function(response) {



        });
};

var sendStatsInfo = function(eventType) {
    chrome.runtime.sendMessage(
        {
            type: 'stats',
            event: eventType
        },
        function(response) {}
    )
};

///Gets the auth from the localstorage
function getAuth() {
    var jsonauth = localStorage.getItem('myUser');
    if(jsonauth) {
        _auth = JSON.parse(jsonauth).result.auth
        return _auth
    } else {
        return null;
    }

}

function getUserId() {
    var _user_id = localStorage.getItem('myUser') || false;
    if(_user_id) {
        _userid = JSON.parse(_user_id).result.user.id;
        return _userid
    } else {
        return null;
    }
}
//Get the broadcast id from the url
function getBroadcastId() {
    return meerkat_id = document.URL.split('/')[4];
}

var _html =
    '<button id="ext_random" type="button">Boring? Random Meerkat!</button>'
    +
    '<button id="ext_follow" type="button">Interesting? Follow '+$('#user-handle').text()+'!</button>';


function rotateVideo(direction) {

    var streamwidth = $('#stream').outerWidth();
    var streamheight = $('#stream').outerHeight();
    var lastMove;

    switch(direction) {
        case 'left':
            $('#stream').css({
                '-webkit-transform':'rotate(-90deg)',
                '-moz-transform':'rotate(-90deg)',
                '-o-transform':'rotate(-90deg)',
                '-ms-transform':'rotate(-90deg)',
                'transform':'rotate(-90deg)',
                '-webkit-transform-origin':'15% 50%',
            });
            $('#top-labels, #watchers, #bottom-labels').css('display', 'none')
                .css('visibility','hidden');

            break;
        case 'right':

            $('#stream').css({
                '-webkit-transform':'rotate(+90deg)',
                '-moz-transform':'rotate(+90deg)',
                '-o-transform':'rotate(+90deg)',
                '-ms-transform':'rotate(+90deg)',
                'transform':'rotate(+90deg)',
                '-webkit-transform-origin':'50% 30%',
            });
            $('#top-labels, #watchers, #bottom-labels').css('display', 'none')
                .css('visibility','hidden');

            lastMove = 'right'

            break;
        case 'reset':
            $('#webchat').css("margin-left","0")

            if (lastMove == 'left') {
                correction = "rotate(+90deg)"
            } else if (lastMove == "right") {
                correction = "rotate(-90deg)"
            } else {
                correction = "rotate(0deg)"
            }

            $('#stream').css({
                '-webkit-transform': correction,
                '-moz-transform': correction,
                '-o-transform': correction,
                '-ms-transform': correction,
                'transform':correction,
                '-webkit-transform-origin':'50% 50%',
                'margin-right': '0'
            });

            $('#top-labels, #watchers, #bottom-labels').css('display', 'block')
                .css('visibility','visible');

            break;
        case 'resize':

            $('#webchat').css("margin-left","0")

            if (lastMove == 'left') {
                correction = "rotate(+90deg)"
            } else if (lastMove == "right") {
                correction = "rotate(-90deg)"
            } else {
                correction = "rotate(0deg)"
            }

            $('#stream').css({
                '-webkit-transform': correction,
                '-moz-transform': correction,
                '-o-transform': correction,
                '-ms-transform': correction,
                'transform':correction,
                '-webkit-transform-origin':'50% 50%',
                'margin-right': '0'
            });

            $('#top-labels, #watchers, #bottom-labels').css('display', 'block')
                .css('visibility','visible');

            rotateVideo('reset');
            $('#rotateRight,#hideCaptions').css({"visibility":""});
            $('#rotateLeft,#hideCaptions').css({"visibility":""});
            $('#rotateReset').hide();
            break;
    }

    jsUpdateSizeWebchat();
}

function jsUpdateSizeWebchat(){

    if (window.innerWidth > 600) {

        var streamHeight = document.getElementById("webchat").offsetHeight;// + 6;
        streamHeight += streamHeight * 0.05
        document.getElementById("webchat").style.width = (9 * streamHeight) / 16 + "px";

    } else {
        document.getElementById("webchat").style.width = "100%";
    }

    document.getElementById("webchat").style.visibility = "visible";
    $("#webchat").fadeIn("slow");

    var pos = $('#stream').offset();

    var width = $('#stream').outerWidth();

    $('#stream').css('margin-left',$(window).width() / 2 - $('#stream').width()+'px');
    $('#stream').css('float',"left");

};


function embedRotateButtons() {
    _hidden = false;

    _rotateHtml =
        '<div class="ext_rotate">' +
        '<span id="rotateRight" class="counterGroup rotatelabel"><span class="counter">rotate right»</span></span>' +
        '<span id="rotateLeft" class="counterGroup rotatelabel"><span class="counter">«rotate left</span></span>' +
        '<span id="rotateReset" class="counterGroup rotatelabel"><span class="counter">rotate reset</span></span>' +
        '<span id="hideCaptions" class="counterGroup rotatelabel"><span class="counter">hide captions</span></span>' +
        '<span id="muteVideo" class="counterGroup rotatelabel"><span class="counter">mute</span></span>' +
        '</div>';
    $('#webchat').after(_rotateHtml);
    //var _mute = false; //global flag
    $('#muteVideo').click(function(){

        if (!_mute) {
            $(this).text("unmute");
            sendStatsInfo('muteClick');
            _mute = true;
        } else {
            $(this).text('mute');
            sendStatsInfo('unmuteClick');
            _mute = false;
        }
       var actualCode = '(' + function() {
       

        jwplayer(0).setMute();

        } + ')();';
        var script = document.createElement('script');
        script.textContent = actualCode;
        (document.head||document.documentElement).appendChild(script);
        script.parentNode.removeChild(script);





    });

    $('#hideCaptions').click(function() {
        if (!_hidden) {
            console.log('hide!');
            sendStatsInfo('hideCaptionClick');
            $(this).text('show captions');
            $('#top-labels, #watchers, #bottom-labels').css('display', 'none')
                .css('visibility','hidden');
            _hidden = true;
        } else {
            console.log('visible');
            sendStatsInfo('showCaptionClick');
            $(this).text('hide captions');
            $('#top-labels, #watchers, #bottom-labels').css('display', 'block')
                .css('visibility','visible');
            _hidden = false;
        }
    });

    $('#rotateRight').click(function(){
        sendStatsInfo('rotateRightClick');
        rotateVideo('right');
        $('#rotateRight,#hideCaptions').css({"visibility":"hidden"});
        $('#rotateLeft,#hideCaptions').css({"visibility":"hidden"});
        $('#rotateReset').show();
    });
    $('#rotateLeft').click(function(){
        sendStatsInfo('rotateLeftClick');
        rotateVideo('left');
        $('#rotateRight,#hideCaptions').css({"visibility":"hidden"});
        $('#rotateLeft,#hideCaptions').css({"visibility":"hidden"});
        $('#rotateReset').show();
    });

    $('#rotateReset').click(function(){
        sendStatsInfo("rotateResetClick");
        rotateVideo('reset');
        $('#rotateRight,#hideCaptions').css({"visibility":""});
        $('#rotateLeft,#hideCaptions').css({"visibility":""});
        $('#rotateReset').hide();
    })

}



$(document).ready(function() {


    _mute = false;
    $('#muteVideo').text('mute');
    if ($('#stream').length == 0) {
        return false;
    }

    var _user_auth = getAuth();
    var _user_bc_id = getBroadcastId();
    var _user_id = getUserId();

    if(document.getElementById('stream') && _user_auth != null && $('#scheduleStream').text() == '') {



        $('body').after($(_html)); //Adds the html to the page

        $('#userhandle').html($('#user-handle').text());

        openClient({_auth: _user_auth,_broadcast_id:_user_bc_id,_user: _user_id});

        $('#ext_random').click(function() {

            var broadcasts = 'http://resources.meerkatapp.co/broadcasts';

             $.getJSON(broadcasts,function(json) {

                var selected = json.result[Math.floor(Math.random()*json.result.length)];

                $.getJSON('https://resources.meerkatapp.co/users/'+selected.influencers[0].substr(2)+'/profile?v=web',function(userbroadcaster) {

                    // var win = window.open('http://meerkatapp.co/broadcast/'+selected.id, '_blank');
                    //win.focus();
                    sendStatsInfo('random');
                    window.location.href = 'http://meerkatapp.co/'+userbroadcaster.result.info.username+'/'+selected.id;

                });


            });

        });

        $('#ext_follow').click(function(){

            var usernametofollow = $('#user-handle').text();
            chrome.runtime.sendMessage(
                {
                    type: 'openUrl',
                    url: 'http://meerkatapp.co/'+usernametofollow.replace('@','')
                },
                function(response) {

                });

        });

        chrome.runtime.sendMessage(
            {
                type: 'replace',

            },
            function(url) {

                $('body').append('<iframe id="webchat" src="'+url+'homepage.html?b='+getBroadcastId()+'"></div>');

                embedRotateButtons();
                jsUpdateSizeWebchat();
                jsUpdateSizeWebchat();
            });

        //window.onresize = jsUpdateSize;

        $(window).resize(function(){
            //Reset the
            jsUpdateSizeWebchat();
            rotateVideo('resize');
        })

    }

});