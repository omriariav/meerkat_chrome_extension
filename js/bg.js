/**
 * Created by @aloncarmel & @omriariav on 3/27/15.
 * We would love some code review if you think it can be done better, hit us up on twitter. 10x ;)
 */
//Google analytics:
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = chrome.extension.getURL('js/analytics.js')
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-61288714-1']);
var manifest = chrome.runtime.getManifest();
version = manifest.version;
_gaq.push(['_trackEvent', version, 'version'])

var addFollowingToLocalStorage = function(followingArray) {
    localStorage.setItem("followingArrayExt",JSON.stringify(followingArray));
};

var getFollowing = function(userId) {
    var _following_list = [];
    var _following_names_id_array = {};
    if (typeof(userId) == undefined || userId == "0") {
        addFollowingToLocalStorage([]);
        return false;
    }
    var _url = 'http://social.meerkatapp.co/users/' + userId + '/following?v=0.1.2';
    res = $.ajax({
        url: _url,
        async: false
    }).responseText;
    res = JSON.parse(res);
    json = res.result;
    for (var idx=0;idx<json.length;idx++) {
        _following_list.push(json[idx].id);
        _following_names_id_array[json[idx].id] = json[idx].username;
    }
    return [_following_list, _following_names_id_array]
};

var loggedUserId = function() {
    var _user_id = localStorage.getItem("ext_userid")|| false;
    if (!_user_id) {
        return "0"
    }
    return _user_id
};

var notificatiosArrayHandler = function(notificationsArray) {
    var _msg, _btns, _title, _meta, _location, _caption;
    var _id, _urls, _btnsArray = [];
    if (typeof(notificationsArray) != typeof([])) {
        return false;
    }
    if (notificationsArray.length == 0) {
        _msg = "";
        return false;
    } else {
        _id = [notificationsArray[0]['id']];
        _btns = 1;
        _title = "@" + notificationsArray[0]['name'] + " is live right now";
        _msg = "Click to view...";
        _urls = [notificationsArray[0]['url']];
        _meta = $.ajax({
            url: "http://resources.meerkatapp.co/broadcasts/" + notificationsArray[0]['id'] + "/summary",
            async: false
        }).responseText;
        _meta = JSON.parse(_meta);
        _location = _meta.result.location;
        _caption = _meta.result.caption;
        if (_location.length == 0) {
            _location = ""
        } else {
            _location = " from " + _location;
        }
        if (_caption.length == 0) {
            _caption = ""
        } else {
            _caption = $.trim(_caption);
        }
        _title = _title + _location;
        _btnsArray = [];
        for (var idxBtn = 0; idxBtn < _btns; idxBtn++) {
            _btnsArray.push({
                title: "Click here to view!",
                iconUrl: chrome.extension.getURL('icons/16x16.png')
            })
        }
        _btnsArray.push({
            title: "Dismiss!",
            iconUrl: chrome.extension.getURL("icons/dismiss.png")
        });
        var notificationsObj = {
            id: _id,
            message: _caption,
            title: _title + "!",
            btns: _btnsArray,
            urls: _urls
        };
        createNotification(notificationsObj);
    }
};

var openNewTab = function(url) {
    chrome.tabs.query({url: url}, function (tabs) {
        if (tabs.length) {
            chrome.tabs.update(tabs[0].id, {
                url: url,
                active: true
            });
        } else {
            chrome.tabs.create({url: url});
        }
    });
};

var createNotification = function(notificationObj) {
    var _notiId = notificationObj['urls'][0];
    localStorage.setItem("_clickedNotificationId", _notiId);
    chrome.notifications.create(
        _notiId,
        {
            type: 'basic',
            'priority': 0,
            iconUrl: chrome.extension.getURL('icons/48x48.png'),
            title: notificationObj['title'],
            message: notificationObj['message'],
            buttons: notificationObj['btns']
        }, function (notificationId) {
            setTimeout(function () {
                chrome.notifications.clear(notificationId, function () {
                })
            }, 15000);
    });
};

chrome.notifications.onButtonClicked.addListener(function (notifId, btnIdx) {
    var _origin_not_id = localStorage.getItem("_clickedNotificationId") || "";
    if (notifId === _origin_not_id) {
        if (btnIdx == 0) {
            _gaq.push(['_trackEvent', 'notificationClickedToView', 'notifciation']);
            openNewTab(_origin_not_id);
        } else if (btnIdx == 1) {
            _gaq.push(['_trackEvent', 'notificationDismissed', 'notifciation']);
            chrome.notifications.clear(notifId, function () {
            })
        }
    }
});

var cacheLiveBroadcasts = function(_cashed_notifications) {
    localStorage.setItem("_cashed_notifications", JSON.stringify(_cashed_notifications))
};

var clearCache = function() {
    var _cache = JSON.parse(localStorage.getItem("_cashed_notifications")) || [];
    if (_cache.length > 100) {
        localStorage.setItem("_cashed_notifications","[]")
    }
};

function worker() {

    $.getJSON('http://resources.meerkatapp.co/broadcasts', function (json) {
        var _notificationFlag = localStorage.getItem("_notificationSettings") || '1';
        if (_notificationFlag == "0") {
            return false;
        }
        var _flag_notification = true;
        var myuserid = loggedUserId();
        if(myuserid == "0") {
            _flag_notification = false;
        }
        if (_flag_notification) {
            var _meta_get_data = getFollowing(myuserid);
            var _storedFollowingUsers = _meta_get_data[0];
            var _following_names_id_array = _meta_get_data[1];
            var _cashed_notifications = JSON.parse(localStorage.getItem("_cashed_notifications")) || [];
            var _notificationsArray = [];
            for (var i = 0; i < json.result.length; i++) {
                var _broadcast_id = json.result[i].id;
                var _flag = true;
                if ($.inArray(_broadcast_id, _cashed_notifications) != -1) {
                    var _flag = false;
                }
                if (_flag) {
                    var _broadcaster = json.result[i].influencers[0].split(":")[1]
                    if ($.inArray(_broadcaster, _storedFollowingUsers) != -1) {
                        _notificationsArray.push({
                            id: _broadcast_id,
                            name: _following_names_id_array[_broadcaster],
                            url: "http://meerkatapp.co/" + _following_names_id_array[_broadcaster]
                            + "/" + _broadcast_id
                        });
                        _cashed_notifications.push(_broadcast_id);
                        cacheLiveBroadcasts(_cashed_notifications);
                    }
                }
            }
            if (_notificationsArray.length > 0) {
                notificatiosArrayHandler(_notificationsArray);
            }
        }
    });
}

worker();
clearCache();
setInterval(function() {
    _notificationFlag = localStorage.getItem("_notificationSettings") || '1';
    if (_notificationFlag == "0") {
        return false;
    }
    worker();
    clearCache();
}, 20000);


chrome.runtime.onInstalled.addListener(function() {
    localStorage.setItem("_notificationSettings","1");
    var isInstalled = localStorage.getItem('_installFlag') || '0';
    if (isInstalled == '0') {
        localStorage.setItem('_installFlag','1');
        chrome.tabs.create({url: 'http://meerkatapp.co/'},function(tab){

            chrome.tabs.executeScript(tab.id, {code: "var user = JSON.parse(localStorage.getItem('myUser')) || []; if(user) {window.location.href = 'http://meerkatapp.co/'+user.result.user.username;}"});

        });
    };
});

chrome.browserAction.onClicked.addListener(function () {
    var broadcasts = 'http://resources.meerkatapp.co/broadcasts';
   
    $.getJSON(broadcasts,function(json) {

        var selected = json.result[Math.floor(Math.random()*json.result.length)];

        $.getJSON('https://resources.meerkatapp.co/users/'+selected.influencers[0].substr(2)+'/profile?v=web',function(userbroadcaster) {
        
             chrome.tabs.create({url: 'http://meerkatapp.co/'+userbroadcaster.result.info.username+'/'+selected.id});
              

        });


    });
});

//user-agent spoof
chrome.webRequest.onBeforeSendHeaders.addListener(
    function(details) {
        for (var i = 0; i < details.requestHeaders.length; ++i) {
            if (details.requestHeaders[i].name == 'User-Agent') {
                details.requestHeaders[i].value = "MeerkatPlus/1.3.3 (Chrome Extension)";
                break;
            }
        }
        return {requestHeaders: details.requestHeaders};
    },
    {urls: ["*://*.meetkatapp.co/*"]},
    ["blocking", "requestHeaders"]);

//background script is waiting for a content script action to open the homepage.html
//Saving the auth and id in local storage for homepage.html use
//Could be done in session or via url parameters as well (i chose local storage
//just to get it done)
chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    switch(message.type) {
        case 'broadcastdata':
            localStorage.setItem("ext_auth", message.auth);
            localStorage.setItem("ext_userid", message.userid);
        break;
        case 'replace':
             sendResponse(chrome.extension.getURL("/"));
        break;
        case 'stats':
            _gaq.push(['_trackEvent', message.event, 'clicked']);
        break;
        case 'openUrl':
            _profile_url = message.url;
            chrome.tabs.query({url: _profile_url}, function(tabs) {
                if (tabs.length) {
                    chrome.tabs.update(tabs[0].id, {url: _profile_url,active: true});
                } else {
                    chrome.tabs.create({url: _profile_url});
                }
            });
        break;
        case 'options':
            var _options_page = chrome.extension.getURL('options.html');
            chrome.tabs.query({url: _options_page}, function(tabs) {
                if (tabs.length) {
                    chrome.tabs.update(tabs[0].id, {url: _options_page,active: true});
                } else {
                    chrome.tabs.create({url: _options_page});
                }
            });
        break;
    }


});





