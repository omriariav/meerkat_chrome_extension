var _online_check_html = "<div class='userlive'>" +
                "<span id='userStatus'></span>" +
                "<span id='userLocation'></span>" +
            "</div>";

function showOnlineStatus(result,info) {
    $('#profile').append($(_online_check_html));
    if (result) {
        Resources.getBroadcast(info.id, function(data) {
            data = data.result;
            console.log(data);
            $('.userlive')
                .click( function() {
                    var win = window.open('http://meerkatapp.co/' + data.broadcaster.name + "/" + data.id);
                    win.focus();
                });
            if (data.location != "") {
                $('#userLocation').text(' From: ' + data.location);
            }
            $('#userStatus')
                .html('I\'m currently online! <span class="meerkatbtn broadcastbtn">Click here to watch my broadcast!</span>');
        })
    } else {
        $('#userStatus')
            .text("I'm currently offline")
            .css('color','#656565');
    }
}

function getUserBroadcastInfo(user_id) {
    Resources.getLiveBroadcastForUser()

}

$(document).ready(function () {
    if ($('#profile').length == 0 && ($('#profile-picture').length == 0)) {
        console.log('not a profile page');
        return false
    }

    var _username_string = $('#handle').text().replace("@","");
    Resources.getUserIdByName(_username_string, function(result) {
        var _user_id = result;
        Resources.getUserLiveBroadcast(result, function(result,info) {
            showOnlineStatus(result, info)
        })
    });

});
